# Warmongr

Searchable list of bribetakers and warmongers. More info on https://acf.international.

[![Build Status](https://img.shields.io/github/actions/workflow/status/sirekanian/warmongr/build.yml?label=build)](https://github.com/sirekanian/warmongr/actions/workflows/build.yml)
[![Tests Status](https://img.shields.io/github/actions/workflow/status/sirekanian/warmongr/tests.yml?label=tests)](https://github.com/sirekanian/warmongr/actions/workflows/tests.yml)

<picture>
  <source media="(prefers-color-scheme: dark)" srcset="app/src/main/play/listings/en-US/graphics/phone-screenshots/1.png">
  <img src="app/src/main/play/listings/en-US/graphics/phone-screenshots/3.png">
</picture>

<picture>
  <source media="(prefers-color-scheme: dark)" srcset="app/src/main/play/listings/en-US/graphics/phone-screenshots/2.png">
  <img src="app/src/main/play/listings/en-US/graphics/phone-screenshots/4.png">
</picture>

<a href='https://play.google.com/store/apps/details?id=com.sirekanian.warmongr'><img height='100' alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>
